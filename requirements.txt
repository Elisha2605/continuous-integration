asgiref==3.5.2
CacheControl==0.12.11
certifi==2022.9.24
charset-normalizer==2.1.1
commonmark==0.9.1
coverage==6.4.4
cyclonedx-python-lib==3.1.0
Django==4.1.3
html5lib==1.1
idna==3.4
lockfile==0.12.2
msgpack==1.0.4
packageurl-python==0.10.4
packaging==21.3
pip-api==0.0.30
pip-requirements-parser==31.2.0
pip_audit==2.4.5
psycopg2-binary==2.9.5
Pygments==2.13.0
pyparsing==3.0.9
pytz==2022.2.1
requests==2.28.1
resolvelib==0.9.0
rich==12.6.0
six==1.16.0
sortedcontainers==2.4.0
sqlparse==0.4.2
toml==0.10.2
urllib3==1.26.12
webencodings==0.5.1
